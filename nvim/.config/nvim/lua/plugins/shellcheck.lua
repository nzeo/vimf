-- https://github.com/pablos123/shellcheck.nvim

return {
  "pablos123/shellcheck.nvim",
  config = function() require "shellcheck-nvim".setup {} end,
}
